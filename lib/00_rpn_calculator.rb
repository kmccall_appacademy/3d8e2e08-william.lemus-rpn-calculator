class RPNCalculator

  OPS = '+-/*'
  attr_accessor :array
  def initialize
    @array = []
  end

  def push(number)
    array.push(number)
  end

  def empty?
    array.empty?
  end

  def operate(sym)
    raise 'calculator is empty' if empty?
    num1 = array.pop(2).map(&:to_f)
    array.push(num1.reduce(sym))
  end

  def plus
    operate(:+)
  end

  def minus
    operate(:-)
  end

  def times
    operate(:*)
  end

  def divide
    operate(:/)
  end

  def tokens(expr)
    expr.split.map do |ch|
      if op?(ch)
        ch.to_sym
      else
        ch.to_i
      end
    end
  end

  def evaluate(expr)
    expr = tokens(expr)
    expr.each do |el|
      if op?(el)
        operate(el)
      else
        array << el
      end
    end
    value
  end

  def op?(ch)
    OPS.include?(ch.to_s)
  end

  def value
    array.last
  end

end
